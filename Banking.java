import java.util.Scanner;

public class Banking {
	public static void main(String[] args) {
		System.out.println("*****Welcome to the Ceylon banking system*****");
		System.out.println("Enter your current balance  :");

		Scanner sc = new Scanner(System.in);
		double CurBal = sc.nextDouble();

		System.out.println("number of times you want to do deposits :");
		System.out.println("---Maximum 5 times u can deposit your valueable money---");
		int DepTimes = sc.nextInt();
		double total1 = 0;
		double total2 = 0;
		do {

			for (int a = 1; a <= DepTimes; a++) {

				System.out.println("Enter your deposite amount  :");
				double dep = sc.nextDouble();

				total1 += dep;

			}
		} while (DepTimes <= 5);
		System.out.println("number of times you want to do withdrawals :");
		int WithTimes = sc.nextInt();

		for (int b = 1; b <= WithTimes; b++) {

			System.out.println("Enter your deposite amount  :");
			double WithDraw = sc.nextDouble();

			total2 += WithDraw;

		}

		double CloBal = ((CurBal + total1) - total2);
		System.out.println("***The closing balance is " + CloBal);

	}
}
