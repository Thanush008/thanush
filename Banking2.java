import java.util.Scanner;

public class Banking {
	public static void main(String[] args) {

		Scanner sc = new Scanner(System.in);

		int DepTimes;
		int WithTimes;
		double CurBal;
		double dep;
		double WithDraw;

		System.out.println("*****Welcome to the Ceylon banking system*****");

		do {
			System.out.println("Enter your current balance  :");
			CurBal = sc.nextDouble();
			if (CurBal < 0) {
				System.out.println("minus values are not accepted!!!");
			}
		} while (CurBal < 0);

		do {
			do {
				System.out.println("!!!Maximum 5 times u can deposit your valueable money!!!");
				System.out.println("number of times you want to do deposits :");

				DepTimes = sc.nextInt();

				if (DepTimes > 5) {
					System.out.println("sorry u can't deposit more than 5 times!!!");
				}
				if (DepTimes < 0) {
					System.out.println("minus values are not accepted!!!");
				}
			} while (DepTimes < 0);

		} while (DepTimes > 5);

		for (int a = 1; a <= DepTimes; a++) {
			do {
				System.out.println("Enter your deposite amount  :");
				dep = sc.nextDouble();
				if (dep < 0) {
					System.out.println("minus values are not accepted!!!");

				}
			} while (dep < 0);
			CurBal += dep;

		}

		do {
			do {
				System.out.println("!!!Maximum 5 times u can withdraw your valueable money!!!");
				System.out.println("number of times you want to do withdrawals :");

				WithTimes = sc.nextInt();

				if (WithTimes > 5) {
					System.out.println("sorry u can't Withdraw more than 5 times!!!");
				}
				if (WithTimes < 0) {
					System.out.println("minus values are not accepted!!!");
				}
			} while (WithTimes < 0);
		} while (WithTimes > 5);

		for (int b = 1; b <= WithTimes; b++) {

			do {
				do {
					System.out.println("Enter your deposite amount  :");
					WithDraw = sc.nextDouble();
					if (WithDraw < 0) {
						System.out.println("minus values are not accepted!!!");
					}
					if (WithDraw > CurBal) {
						System.out.println("sorry,insatisfied balance!!!");
					}

				} while (WithDraw > CurBal);
			} while (WithDraw < 0);
			CurBal -= WithDraw;

		}

		System.out.println("The closing balance is " + CurBal);
		System.out.println("***thank you for using***");

	}

}
