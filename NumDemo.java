import java.util.Scanner;

public class NumDemo {
	public static void main(String[] args) {

		int i;
		boolean flag = true;
		System.out.println("enter your number :");
		Scanner Sc = new Scanner(System.in);

		int num = Sc.nextInt();
		for (i = 2; i <= num / 2; i++) {
			System.out.println(i);
			if (num % i == 0) {

				flag = false;
				break;
			}
		}

		if (flag == true)
			System.out.println("prime num");

		else
			System.out.println("not");
	}
}