import java.util.Scanner;

public class NumDemo {
	// class define
	public static void main(String[] args) {
		// main method define

		int i;
		// integer variable going to change with loop
		boolean flag = true;
		// flag is a boolean variable
		System.out.println("enter your number :");
		Scanner Sc = new Scanner(System.in);
		// user input

		int num = Sc.nextInt();
		for (i = 2; i <= num / 2; i++) {
			// to change number value of i in loop condition until Its going to end the loop

			if (num % i == 0) {
				// main condition

				flag = false;
				// condition for break the code
				break;
			}
		}

		if (flag == true)
			// user output
			System.out.println("prime num");

		else // user output
			System.out.println("not");
	}
}